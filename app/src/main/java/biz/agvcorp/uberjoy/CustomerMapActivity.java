package biz.agvcorp.uberjoy;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CustomerMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraMoveCanceledListener, GoogleMap.OnCameraIdleListener, RoutingListener, GoogleMap.OnMapClickListener{
    private FirebaseAuth mAuth;

    private GoogleMap mMap;
    FusedLocationProviderClient mFusedLocationClient;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LatLng mMovedLocation = null;
    LatLng driverLocationLatLng = null;
    double mMovedZoom = 0, mMovedTilt = 0, mMovedBearing = 0;
    LocationRequest mLocationRequest;
    private int tripEnder = 0;

    private LatLng destinationLatLng;

    private SupportMapFragment mapFragment;
    private Button mLogout, mRequest, mSettings, mRecord;
    private LatLng pickupLocation;
    private Boolean requestBol = false;
    private Marker pickupMarker;
    private Marker driverCarMarker;
    private Marker setDestinationMarker;

    private LinearLayout mDriverInfo;
    private ImageView mDriverProfileImage;
    private TextView mDriverName, mDriverPhone, mDriverLocation, mDriverCar;

    private RadioGroup mRadioGroup;

    private String destination, requestService;

    private static int[] COLORS = new int[]{R.color.colorCustomerMapRoute};
    private static int routeDistance;
    private static int routeDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_map);
        mAuth = FirebaseAuth.getInstance();

        destinationLatLng = new LatLng(0.0, 0.0);
        mMovedLocation = new LatLng(0.0, 0.0);
        driverLocationLatLng = new LatLng(0.0, 0.0);
        if (mMovedLocation != null) {
            mMovedLocation = null;
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        polylines = new ArrayList<>();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
            mapFragment.getMapAsync(this);
        }

        mLogout = findViewById(R.id.logout);
        mRequest = findViewById(R.id.request);
        mSettings = findViewById(R.id.setting);
        mRecord = findViewById(R.id.record);

        mDriverInfo = findViewById(R.id.driverInfo);
        mDriverProfileImage = findViewById(R.id.driverProfileImage);
        mDriverName = findViewById(R.id.driverName);
        mDriverPhone = findViewById(R.id.driverPhone);
        mDriverLocation = findViewById(R.id.driverLocation);
        mDriverCar = findViewById(R.id.driverCar);

        mRadioGroup = findViewById(R.id.radioGroup);
        mRadioGroup.check(R.id.JoyCar);

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id = mAuth.getCurrentUser().getUid();
                FirebaseAuth.getInstance().signOut();

                if (user_id != null){
                    DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);
                    current_user_db.removeValue();
                }

                Intent intent = new Intent(CustomerMapActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });
        mRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (requestBol){
                    tripEnder = 1;
                    registerEnder();
                    endRide();
                } else {
                    int selectId = mRadioGroup.getCheckedRadioButtonId();
                    final RadioButton radioButton = (RadioButton) findViewById(selectId);
                    if (radioButton.getText() == null){
                        return;
                    }
                    requestService = radioButton.getText().toString();

                    requestBol = true;
                    String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("customerRequest");

                    GeoFire geoFire = new GeoFire(ref);
                    geoFire.setLocation(userId, new GeoLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude()));

                    pickupLocation = new LatLng( mLastLocation.getLatitude(), mLastLocation.getLongitude() );
                    pickupMarker = mMap.addMarker(new MarkerOptions().position(pickupLocation).title("This is your Pickup Point").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_marker_last)) );

                    mRequest.setText("We are Getting a Driver for You ...");
                    getClosestDriver();
                }
            }
        });
        mSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerMapActivity.this, CustomerSettingsActivity.class);
                startActivity(intent);
                return;
            }
        });

        final PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                destination = place.getName().toString();
                destinationLatLng = place.getLatLng();
                mRequest.setVisibility(View.VISIBLE);
                drawRouteSelectedPlace();
            }
            @Override
            public void onError(Status status) {
                mRequest.setVisibility(View.GONE);
            }
        });
        autocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
            ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Please Search First To Call Taxi");
            mRequest.setVisibility(View.GONE);
            erasePolylines();
            }
        });

        mRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(CustomerMapActivity.this, RecordActivity.class);
//                intent.putExtra("customerOrDriver", "AllCustomersInfo");
//                startActivity(intent);
//                return;
            }
        });
    }

    private void drawRouteSelectedPlace(){
        erasePolylines();
        COLORS = new int[]{R.color.colorCustomerSearchMapRoute};
        getRouteToMarker(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), destinationLatLng);
    }

    private void drawRouteToPickupPoint(){
        erasePolylines();
        COLORS = new int[]{R.color.colorCustomerToPickup};
        getRouteToMarker(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), driverLocationLatLng);
    }


    private int radius = 1;
    private Boolean driverFound = false;
    private String driverFoundID;
    GeoQuery geoQuery;

    private void getClosestDriver(){
//        this is number one
        DatabaseReference driverLocation = FirebaseDatabase.getInstance().getReference().child("DriversAvailable");
        GeoFire geoFire = new GeoFire(driverLocation);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(pickupLocation.latitude, pickupLocation.longitude), radius);
        geoQuery.removeAllListeners();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) { //This will fire when a driver is found from DriversAvailable
                if (!driverFound && requestBol){
                    //Checking IF the Driver is at right Service as the customer needs
                    DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("AllDriversInfo").child(key);
                    mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                                Map<String, Object> driverMap = (Map<String, Object>) dataSnapshot.getValue();
                                if (driverFound){
                                    return;
                                }

                                if (driverMap.get("service").equals(requestService) ){
                                    driverFound = true;
                                    driverFoundID = dataSnapshot.getKey();

                                    DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(driverFoundID).child("customerRequest");
                                    String customerId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    HashMap map = new HashMap();
                                    map.put("customerRideId", customerId);
                                    map.put("destination", destination);
                                    map.put("destinationLat", destinationLatLng.latitude);
                                    map.put("destinationLng", destinationLatLng.longitude);
                                    driverRef.updateChildren(map);

                                    mRequest.setText("Showing the Driver Location ...");
                                    getDriverLocation();
                                    getDriverInfo();
                                    getHasRideEnded();
                                    getTripEnderInfo();
                                    getCustomerIsPicked();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Sorry. We currently have no Driver near your location at your desired service. Please Try again after sometime!", Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                if (!driverFound){
                    radius++;
                    getClosestDriver();
                }
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    //    private Marker mDriverMarker;
    private DatabaseReference driverLocationRef;
    private ValueEventListener driverLocationRefListener;
    private void getDriverLocation(){
        //        this is four
        driverLocationRef = FirebaseDatabase.getInstance().getReference().child("DriversWorking").child(driverFoundID).child("l");
        driverLocationRefListener = driverLocationRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && requestBol){
                    List<Object> map = (List<Object>) dataSnapshot.getValue();
                    double locationLat = 0;
                    double locationLng = 0;
                    mRequest.setText("Driver Found");
                    if(map.get(0) != null){
                        locationLat = Double.parseDouble(map.get(0).toString());
                    }
                    if(map.get(1) != null){
                        locationLng = Double.parseDouble(map.get(1).toString());
                    }
                    LatLng driverLatLng = new LatLng(locationLat,locationLng);
                    driverLocationLatLng = driverLatLng;
                    if(driverCarMarker != null){
                        driverCarMarker.remove();
                    }

                    Location loc1 = new Location("");
                    loc1.setLatitude(pickupLocation.latitude);
                    loc1.setLongitude(pickupLocation.longitude);
                    Location loc2 = new Location("");
                    loc2.setLatitude(driverLatLng.latitude);
                    loc2.setLongitude(driverLatLng.longitude);
                    float distance = loc1.distanceTo(loc2);

                    if (distance < 100){
                        mRequest.setText("Your Driver is Here !");
                    } else {
                        mRequest.setText("Driver Comming. Still " + String.valueOf(distance) + " meters away");
                    }

                    driverCarMarker = mMap.addMarker(new MarkerOptions().position(driverLatLng).title("This is your driver").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car_2_map)) );
                    drawRouteToPickupPoint();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private DatabaseReference driveHasEndedRef;
    private ValueEventListener driveHasEndedRefListener;
    private void getHasRideEnded(){
        driveHasEndedRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(driverFoundID).child("customerRequest").child("customerRideId");
        driveHasEndedRefListener = driveHasEndedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                }else{
                    endRide();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private DatabaseReference customerisPickedRef;
    private ValueEventListener customerisPickedRefListener;
    private void getCustomerIsPicked(){
        final String customerId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        customerisPickedRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(driverFoundID).child("customerRequest");
        customerisPickedRefListener = customerisPickedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("pickedCustomerId")!=null){
                        askCustomeraboutPickup();
                        getRouteToMarker(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), destinationLatLng);
                    }
                }else{

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void askCustomeraboutPickup(){
        Toast.makeText(getApplicationContext(), "Please Confirm that You have boarded the vehicle you calling for!", Toast.LENGTH_LONG).show();
    }

    /*-------------------------------------------- getDriverInfo -----
    |  Function(s) getDriverInfo
    |
    |  Purpose:  Get all the user information that we can get from the user's database.
    |
    |  Note: --
    |
    *-------------------------------------------------------------------*/
    private void getDriverInfo(){
        mDriverInfo.setVisibility(View.VISIBLE);
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("AllDriversInfo").child(driverFoundID);
        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("name")!=null){
                        mDriverName.setText("Driver Name: "+map.get("name").toString());
                    }
                    if(map.get("phone")!=null){
                        mDriverPhone.setText("Driver Phone No: "+map.get("phone").toString());
                    }
                    if(map.get("car")!=null){
                        mDriverCar.setText("Driver Car Name: "+map.get("car").toString());
                    }
                    if(map.get("profileImageUrl")!=null){
                        Glide.with(getApplication()).load(map.get("profileImageUrl").toString()).into(mDriverProfileImage);
                    }
                } else {
                    mDriverName.setText("Driver Name: Not Configured");
                    mDriverPhone.setText("Driver Phone No: Not Configured");
                    mDriverCar.setText("Driver Car Name: Not Configured");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void getRouteToMarker(LatLng pickupLatLng, LatLng destinationLatLng) {
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
//                .waypoints(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), pickupLatLng)
                .waypoints(destinationLatLng, pickupLatLng)
                .build();
        routing.execute();
    }

    private void getTripEnderInfo(){
        DatabaseReference mTripEnderDatabase = FirebaseDatabase.getInstance().getReference().child("AllDriversInfo").child(driverFoundID);
        mTripEnderDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("TripEnder")!=null){
                        switch ( map.get("TripEnder").toString() ){
                            case "completed":
                                mRequest.setText("Driver Completed Trip ! Stand By");
                                break;
                            case "cancelled":
                                mRequest.setText("Driver Cancelled Trip ! Stand By");
                                break;
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Some Server Error Occured. Please Stand By", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void registerEnder(){
        String customerId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference mTripEnderRegisterDatabase = FirebaseDatabase.getInstance().getReference().child("AllCustomersInfo").child(customerId);
        HashMap map = new HashMap();

        switch (tripEnder){
            case 1:
                map.put("TripEnder", "completed");
                break;
            case 2:
                map.put("TripEnder", "cancelled");
                break;
        }
        mTripEnderRegisterDatabase.updateChildren(map);
    }

    private void endRide(){
        requestBol = false;
        erasePolylines();
        geoQuery.removeAllListeners();
        driverLocationRef.removeEventListener(driverLocationRefListener);
        driveHasEndedRef.removeEventListener(driveHasEndedRefListener);
        customerisPickedRef.removeEventListener(customerisPickedRefListener);

        if (driverFoundID != null){
            DatabaseReference driverRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(driverFoundID);
            driverRef.setValue(true);
            driverFoundID = null;
        }

        driverFound = false;
        radius = 1;

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("customerRequest");
        GeoFire geoFire = new GeoFire(ref);
        geoFire.removeLocation(userId);

        if (pickupMarker != null){
            pickupMarker.remove();
        }
        if (driverCarMarker != null){
            driverCarMarker.remove();
        }
        if (setDestinationMarker != null){
            setDestinationMarker.remove();
        }

        mRequest.setText("Ride Canceled. Ready to Call Again !!");
        driverInfoDelete();
    }

    private void driverInfoDelete(){
        mDriverInfo.setVisibility(View.GONE);
        mDriverName.setText("");
        mDriverPhone.setText("");
        mDriverCar.setText("Driver's Car: ...");
        mDriverProfileImage.setImageResource(R.mipmap.ic_profile_image);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);
        mMap.setOnMapClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);

        

//        final SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        smf.getView().findViewById(0x2).performClick();
        mMovedZoom = 0;
        mMovedTilt = 0;
        mMovedBearing = 0;
        if (mMovedLocation != null){
            mMovedLocation = new LatLng(0.0, 0.0);
        }
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
//                Log.i("CustomerMapActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                updateMap(location);
            }
        };
    };

    @Override
    public void onLocationChanged(Location location) {
//        Log.i("CustomerMapActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
        //updateMap(location);
    }

    public void updateMap(Location location){
        mLastLocation = location;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (mMovedLocation != null){
            if (mMovedLocation.latitude == 0.0 && mMovedLocation.longitude == 0.0){
                mMovedLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }
        }

        if (mMovedLocation != null){
            LatLng movedLatlng = new LatLng(mMovedLocation.latitude, mMovedLocation.longitude);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(movedLatlng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) mMovedZoom));
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }

//        if (pickupMarker != null){
//            pickupMarker.remove();
//        }
//        pickupMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("You Are Here !").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_marker_last)) );
//        Log.i("MyOriginalPickup", latLng.toString());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestCurLocationUpdates();
    }

    public void requestCurLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mFusedLocationClient != null) {
            requestCurLocationUpdates();
        } else {
            buildGoogleApiClient();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    final int LOCATION_REQUEST_CODE = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case LOCATION_REQUEST_CODE:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    mapFragment.getMapAsync(this);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Give the Permission to Use the App any Further", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {
        CameraPosition newLocation = mMap.getCameraPosition();
        mMovedLocation = newLocation.target;
        mMovedZoom = newLocation.zoom;
        mMovedTilt = newLocation.tilt;
        mMovedBearing = newLocation.bearing;
        Log.d("NewMovedCameraPosition", newLocation.toString() );
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
//            Toast.makeText(this, "The user gestured on the map.", Toast.LENGTH_SHORT).show();
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {
//            Toast.makeText(this, "The user tapped something on the map.", Toast.LENGTH_SHORT).show();
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {
//            Toast.makeText(this, "The app moved the camera.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        if(e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    private List<Polyline> polylines;
    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if(polylines.size()>0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

            routeDistance = route.get(i).getDistanceValue();
            routeDuration = route.get(i).getDurationValue();
            Toast.makeText(getApplicationContext(),"Route "+ (i+1) +": distance - "+ route.get(i).getDistanceValue()+": duration - "+ route.get(i).getDurationValue(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    private void erasePolylines(){
        try {
            for (Polyline line: polylines){
                line.remove();
            }
            polylines.clear();
        } catch (NullPointerException err){
            return;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        try {
            List<Address> add =  getPlaceDetails(latLng);
            Toast.makeText(getApplicationContext(), "You Picked the location " + add.get(0).getAddressLine(0), Toast.LENGTH_LONG).show();
            putDestinationMarker(latLng);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void putDestinationMarker(LatLng latLng){
        if (setDestinationMarker != null){
            setDestinationMarker.remove();
        }
        setDestinationMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("This is your driver").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_destination_marker)) );
    }

    private List<Address> getPlaceDetails(LatLng latLng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        return addresses;
    }
}
